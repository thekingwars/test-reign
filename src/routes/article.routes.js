import { Router } from "express";
import {
  createArticle,
  deleteArticlesBD,
  getArticlesBD,
} from "../controller/article.controller";
const router = Router();

/**
 * @swagger
 * components:
 *  schemas:
 *    Article:
 *      type: object
 *      properties:
 *          created_at:
 *              type: string
 *              format: date
 *          title:
 *              type: string
 *          url:
 *              type: string
 *          author:
 *              type: string
 *          points:
 *              type: integer
 *          story_text:
 *              type: string
 *          comment_text:
 *              type: string
 *          num_comments:
 *              type: integer
 *          story_id:
 *              type: integer
 *          story_title:
 *              type: string
 *          story_url:
 *              type: string
 *          parent_id:
 *              type: integer
 *          created_at_i:
 *              type: integer
 *          _tags:
 *              type: array
 *              description: string array
 *          objectID:
 *              type: string
 *          _highlightResult:
 *              type: object
 *              properties:
 *                  title:
 *                      type: object
 *                      properties:
 *                          value:
 *                              type: string
 *                          matchLevel:
 *                              type: string
 *                          fullyHighlighted:
 *                              type: boolean
 *                          matchedWords:
 *                              type: array
 *                              description: string array
 *                  url:
 *                      type: object
 *                      properties:
 *                          value:
 *                              type: string
 *                          matchLevel:
 *                              type: string
 *                          matchedWords:
 *                              type: array
 *                  author:
 *                      type: object
 *                      properties:
 *                          value:
 *                              type: string
 *                          matchLevel:
 *                              type: string
 *                          matchedWords:
 *                              type: array
 *      required:
 *          - author
 *          - url
 *          - points
 *          - created_at
 *          - num_comments
 *          - objectID
 *          - created_at_i
 *          - _highlightResult
 *          - _tags
 */

/**
 * @swagger
 * /api/create/article:
 *  post:
 *    summary: Allows to create by an http request the following articles
 *    tags:  [Article]
 *    requestBody:
 *      required: false
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            $ref: '#/components/schemas/Article'
 *    responses:
 *      201:
 *        description: article created
 */
router.post("/create/article", createArticle);

/**
 * @swagger
 * /api/articles:
 *  get:
 *    summary: Bring all the articles, if there is any filter filter by request
 *    tags: [Article]
 *    parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *        required: false
 *        description: skip n api elements
 *      - in: query
 *        name: author
 *        schema:
 *          type: string
 *        required: false
 *        description: filter by author
 *      - in: query
 *        name: tags
 *        schema:
 *          type: string
 *        required: false
 *        description: filter by tags
 *      - in: query
 *        name: title
 *        schema:
 *          type: string
 *        description: filters by title, but ignores other filters
 *    responses:
 *      200:
 *        description: all articles
 *        content:
 *          application/json:
 *            type: array
 *            items:
 *              $ref: '#/components/schemas/Article'
 *      404:
 *        description: articles not found
 */
router.get("/articles", getArticlesBD);
/**
 * @swagger
 * /api/delete/article/{_id}:
 *  delete:
 *    summary: delete a article
 *    tags: [Article]
 *    parameters:
 *      - in: path
 *        name: _id
 *        schema:
 *          type: string
 *        required: true
 *        description: the article id
 *    responses:
 *      200:
 *        description: articles deleted
 *      404:
 *        description: articles not found
 */
router.delete("/delete/article/:_id", deleteArticlesBD);

export default router;

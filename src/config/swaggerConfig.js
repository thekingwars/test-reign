import path from "path";

export const swaggerConfig = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "test-reign mongoDB API",
      version: "1.0.0",
    },
    servers: [
      {
        url: "http://localhost:3000/",
      },
    ],
  },
  apis: [`${path.join(__dirname, "../routes/*.js")}`],
};

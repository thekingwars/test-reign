import axios from "axios";
import db from "../db";
import nodeCron from "node-cron";
import { ObjectId } from "mongodb";

export const removeNullItems = async () => {
  try {
    const articles = await axios({
      method: "get",
      url: "https://hn.algolia.com/api/v1/search_by_date?query=nodejs",
    });

    /**
     * I filter the elements to remove all those where the title is null
     */
    const newArticles = articles.data.hits.filter(article => !!article.title);

    return newArticles;
  } catch (err) {
    console.log(err);
  }
};

export const createArticlePerHour = () => {
  /**
   * each hour of the clock will insert data
   */
  let schedule = nodeCron.schedule("0 0 */1 * * *", async () => {
    const articlesNode = await removeNullItems();
    await db.collection("article").insertMany(articlesNode);

    console.log("data successfully inserted");
  });

  return schedule;
};

export const createArticle = async (req, res) => {
  try {
    const articlesNode = await removeNullItems();
    const articlesBD = await db.collection("article").insertMany(articlesNode);

    return res.status(201).json(articlesBD);
  } catch (err) {
    return res.status(500).json(err);
  }
};

export const getArticlesBD = async (req, res) => {
  const { offset, author, tags, title } = req.query;

  let filters = [];

  if (!tags && !author && !title) {
    filters.push({});
  } else {
    filters = [
      { _tags: { $in: [tags] } },
      { author },
      /**
       * If you filter by title, it will ignore the other filters, since it searches directly by title match.
       */
      { title: { $regex: title ? title : "", $options: "$i" } },
    ];
  }

  const paginationArticle = await db
    .collection("article")
    .find({
      $or: filters,
    })
    .skip(Number(offset))
    .limit(5)
    .toArray();

  return res.status(200).json(paginationArticle);
};

export const deleteArticlesBD = async (req, res) => {
  const { _id } = req.params;
  const articleRemoved = await db
    .collection("article")
    .deleteOne({ _id: ObjectId(_id) });

  return res.status(204).json({
    deletedCount: articleRemoved.deletedCount,
    msg: "Article Successfully removed",
  });
};

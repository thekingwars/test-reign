import express from "express";
import cors from "cors";
import morgan from "morgan";
import db from "./db";
import routes from "./routes/article.routes";
import { createArticlePerHour } from "./controller/article.controller";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";
import { swaggerConfig } from "./config/swaggerConfig";

//vars
const app = express();

//options
app.set("port", 3000 || process.env.PORT);

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(
  "/api-doc",
  swaggerUI.serve,
  swaggerUI.setup(swaggerJsDoc(swaggerConfig))
);

//routes
app.use("/api", routes);

//server
const server = app.listen(app.get("port"), () => {
  console.log(`Server on port ${app.get("port")}`);
  createArticlePerHour().start();
});

export { app, server };

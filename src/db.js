import { url } from "./config/db.config";
const { MongoClient } = require("mongodb");

/**
 * Connection URL
 */
const client = new MongoClient(url);

/**
 * Database Name
 */
const dbName = "test-reign";

async function main() {
  /**
   * Use connect method to connect to the server
   */
  await client.connect();
  console.log("Connected successfully to server");
}

main().then(console.log);

export default client.db(dbName);

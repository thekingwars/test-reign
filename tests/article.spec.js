import client from "../src/db";
import { server, app } from "../src/index";
import request from "supertest";
import { articles } from "../src/helpers/articles";

const api = request(app);

beforeEach(async () => {
  const articleCount = await client.collection("article").countDocuments();

  if (articleCount < 2) {
    await client.collection("article").insertMany(articles);
  }
});

describe("GET request article", () => {
  test("should respond with a status 200 and a json", async () => {
    await api
      .get("/api/articles")
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });

  test("a 2-item arrangement should arrive", async () => {
    const response = await api.get("/api/articles");
    expect(response.body.length).toBeGreaterThan(1);
  });
});

describe("POST request article", () => {
  test("should respond with a status 201", async () => {
    const response = await api.post("/api/create/article").send();

    expect(response.headers["content-type"]).toEqual(
      expect.stringContaining("json")
    );
  });

  test("a json should arrive in the header", async () => {
    const response = await api.post("/api/create/article").send();

    expect(response.statusCode).toBe(201);
  });

  test("Several items should be created correctly", async () => {
    const response = await api.post("/api/create/article").send();

    expect(response.body.insertedCount).toBeGreaterThan(0);
  });

  test("There should not be any null or undefined values.", async () => {
    const response = await api.post("/api/create/article").send();

    expect(response.body).not.toBeNull();
    expect(response.body).not.toBeUndefined();
  });
});

describe("DELETE request article", () => {
  test("you should be able to delete an item", async () => {
    const countArticles =
      (await client.collection("article").countDocuments()) - 1;
    const findArticles = await client.collection("article").find({}).toArray();

    const numRandom = Math.floor(Math.random() * countArticles);

    const response = await api.delete(
      `/api/delete/article/${findArticles[numRandom]._id}`
    );

    expect(response.statusCode).toBe(204);
  });
});

afterAll(() => {
  server.close();
});

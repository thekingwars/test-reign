# test-reign
To populate the database there is a path called /api/create/article, it is in charge of inserting an array of articles doing a search on the url given by algolia for the test, however every 1hr of the clock I insert data as requested (it is good to clarify that it does it for every hour of the clock, that is, if it is 11:30 and you close the server and open it at 11:59, at 12 it inserts the data, since it is not a counter but it checks every hour of the clock).

There are 3 endpoints, plus 2 functions, one that is in charge of eliminating the null titles so as not to overload the database and the other one that adds data every hour.

The database is made in pure mongodb, you would have to download it if you don't have it, I already validate that when executing the function by hour or the endpoint to create create the collections so they don't need anything else, and with only starting the server it creates the connection.

## Getting started

install mongoDB
install nodejs
npm i before the test
npm run dev to start development server

## Usage
first if you don't want to wait for the time to insert the data you can run the createArticle which is in the path /api/create/article

Then you can do a GET to the path /api/articles, which by means of params you can add the data you want to filter by

And finally the delete that asks for the objectId of the article, and you will have the process completely ready.
